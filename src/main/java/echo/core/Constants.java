/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core;

public abstract class Constants {

	public static final String BASE_PACKAGE = "echo";

	private Constants() {
		throw new UnsupportedOperationException();
	}
}
