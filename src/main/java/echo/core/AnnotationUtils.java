/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public abstract class AnnotationUtils {

	private AnnotationUtils() {
		throw new UnsupportedOperationException();
	}

	public static String getSinceEmpty(Method method, Class<? extends Annotation> annotationType) {
		return String.format("%s is required on %s#%s", annotationType, method.getDeclaringClass(), method.getName());
	}
}
