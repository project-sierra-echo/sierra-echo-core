/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import echo.core.domain.stereotype.Domain;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import org.springframework.lang.NonNull;

public class DomainDescriptorImpl implements DomainDescriptor {

	private final Class<? extends Domain> domainType;

	private final Map<String, Getter> getters;
	private final Map<String, Setter> setters;

	public DomainDescriptorImpl(Class<? extends Domain> domainType,
			Map<String, Getter> getters,
			Map<String, Setter> setters) {
		this.domainType = domainType;
		this.getters = getters;
		this.setters = setters;
	}

	@NonNull
	@Override
	public Class<? extends Domain> getDomainType() {
		return domainType;
	}

	@NonNull
	@Override
	public Optional<Getter> getGetter(@NonNull String propertyName) {
		return Optional.ofNullable(getters.get(propertyName));
	}

	@NonNull
	@Override
	public Optional<Setter> getSetter(@NonNull String propertyName) {
		return Optional.ofNullable(setters.get(propertyName));
	}

	@NonNull
	@Override
	public Getter requireGetter(@NonNull String propertyName) {
		return getGetter(propertyName)
				.orElseThrow(() -> createException(Getter.class, propertyName));
	}

	@NonNull
	@Override
	public Setter requireSetter(@NonNull String propertyName) {
		return getSetter(propertyName)
				.orElseThrow(() -> createException(Setter.class, propertyName));
	}

	private DomainTypeException createException(Class<? extends PropertyAccessor> accessorClass, String propertyName) {
		return new DomainTypeException(String.format(
				"%s: Unable to resolve %s for property %s",
				domainType, accessorClass, propertyName));
	}

	@NonNull
	@Override
	public List<Getter> requireAllGetters(@NonNull List<String> propertyNames) {
		return requireAllOfProperties(propertyNames, this::requireGetter);
	}

	@NonNull
	@Override
	public List<Setter> requireAllSetters(@NonNull List<String> propertyNames) {
		return requireAllOfProperties(propertyNames, this::requireSetter);
	}

	private <T> List<T> requireAllOfProperties(List<String> propertyNames,
			Function<String, T> producer) {
		return propertyNames.stream()
				.map(producer)
				.toList();
	}
}
