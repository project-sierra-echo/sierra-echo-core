/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import static java.util.Collections.unmodifiableMap;

import echo.core.Constants;
import echo.core.domain.factory.DomainFactoryException;
import echo.core.domain.stereotype.Domain;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.lang.NonNull;
import org.springframework.util.ReflectionUtils;

public class DomainContextImpl implements DomainContext {

	private static final Logger LOGGER = LoggerFactory.getLogger(DomainContextImpl.class);

	private final Map<Class<? extends Domain>, DomainDescriptor> descriptors;

	public DomainContextImpl() {
		final List<Class<? extends Domain>> domainTypes = discoverDomainTypes();

		descriptors = new DescriptorProducer(domainTypes).getDescriptors();
	}

	@SuppressWarnings("unchecked")
	private static Class<? extends Domain> tryParse(BeanDefinition beanDefinition) {
		try {
			return (Class<? extends Domain>) Class.forName(beanDefinition.getBeanClassName());
		} catch (ClassNotFoundException e) {
			throw new DomainTypeException(e);
		}
	}

	@SuppressWarnings("squid:S6204")
	private List<Class<? extends Domain>> discoverDomainTypes() {
		final ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(
				false);

		scanner.addIncludeFilter(new AssignableTypeFilter(Domain.class));

		final List<Class<? extends Domain>> candidates = scanner.findCandidateComponents(Constants.BASE_PACKAGE)
				.stream()
				.map(DomainContextImpl::tryParse)
				.collect(Collectors.toList());

		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("Discovered {} domain type(s)\n\t{}", candidates.size(),
					candidates.stream().map(Class::getName).collect(Collectors.joining(", ")));
		}

		return candidates;
	}

	@NonNull
	@Override
	public Optional<DomainDescriptor> getDescriptor(@NonNull Class<? extends Domain> domainType) {
		return Optional.ofNullable(descriptors.get(domainType));
	}

	@NonNull
	@Override
	public DomainDescriptor requireDescriptor(@NonNull Class<? extends Domain> domainType) {
		return getDescriptor(domainType)
				.orElseThrow(() -> new DomainFactoryException(
						String.format("Unable to locate %s for %s", DomainDescriptor.class, domainType)));
	}

	@NonNull
	@Override
	public List<Class<? extends Domain>> getSubTypes(@NonNull Class<?> domainType) {
		return descriptors.keySet().stream()
				.filter(domainType::isAssignableFrom)
				.toList();
	}

	private record DescriptorProducer(List<Class<? extends Domain>> domainTypes) {

		private static final Logger LOGGER = LoggerFactory.getLogger(DescriptorProducer.class);

		private static boolean nonTransient(Field field) {
			return !Modifier.isTransient(field.getModifiers());
		}

		private static <T extends PropertyAccessor> Map<String, T> finalizeMap(
				Class<? extends Domain> domainType,
				Class<T> accessorType,
				Map<String, T> accessors) {
			LOGGER.trace("Found {} {} reference(s) in {}", accessors.size(), accessorType, domainType);

			return unmodifiableMap(accessors);
		}

		private static DomainDescriptor createDescriptor(Class<? extends Domain> domainType) {
			final Map<String, Getter> getters = new HashMap<>();
			final Map<String, Setter> setters = new HashMap<>();

			ReflectionUtils.doWithFields(domainType,
					field -> putAccessors(domainType, field, getters, setters),
					DescriptorProducer::nonTransient);

			return new DomainDescriptorImpl(domainType,
					finalizeMap(domainType, Getter.class, getters),
					finalizeMap(domainType, Setter.class, setters));
		}

		private static void putAccessors(Class<? extends Domain> domainType,
				Field field,
				Map<String, Getter> getters,
				Map<String, Setter> setters) {
			final String fieldName = field.getName();

			getters.put(fieldName, new Reader(domainType, fieldName));

			if (Modifier.isFinal(field.getModifiers())) {
				return;
			}

			setters.put(fieldName, new Writer(domainType, fieldName));
		}

		private static Map.Entry<Class<? extends Domain>, DomainDescriptor> toEntry(
				DomainDescriptor domainDescriptor) {
			return Map.entry(domainDescriptor.getDomainType(), domainDescriptor);
		}

		private Map<Class<? extends Domain>, DomainDescriptor> getDescriptors() {
			return domainTypes.stream()
					.map(DescriptorProducer::createDescriptor)
					.map(DescriptorProducer::toEntry)
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		}
	}
}
