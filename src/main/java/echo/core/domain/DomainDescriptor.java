/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import echo.core.domain.stereotype.Domain;
import java.util.List;
import java.util.Optional;
import org.springframework.lang.NonNull;

public interface DomainDescriptor {

	@NonNull
	Class<? extends Domain> getDomainType();

	@NonNull
	Optional<Getter> getGetter(@NonNull String propertyName);

	@NonNull
	Optional<Setter> getSetter(@NonNull String propertyName);

	@NonNull
	Getter requireGetter(@NonNull String propertyName);

	@NonNull
	Setter requireSetter(@NonNull String propertyName);

	@NonNull
	List<Getter> requireAllGetters(@NonNull List<String> propertyNames);

	@NonNull
	List<Setter> requireAllSetters(@NonNull List<String> propertyNames);
}
