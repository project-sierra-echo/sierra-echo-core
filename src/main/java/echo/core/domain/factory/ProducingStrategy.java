package echo.core.domain.factory;

import java.util.function.Supplier;
import org.springframework.lang.NonNull;

public interface ProducingStrategy<T> extends Supplier<T> {

	@NonNull
	@Override
	T get();
}
