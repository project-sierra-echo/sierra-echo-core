/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory;

import echo.core.domain.WithDomain;
import echo.core.domain.stereotype.Domain;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FactoryCut {

	WithDomain withDomain() default @WithDomain(Domain.class);

	Cutting cutting() default Cutting.ALL;
}
