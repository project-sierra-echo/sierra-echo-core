/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory;

import echo.core.domain.stereotype.Domain;
import java.util.function.BiFunction;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public interface MappingStrategy<E, T extends Domain> extends BiFunction<E, T, T> {

	@NonNull
	@Override
	T apply(@Nullable E e, @NonNull T t);
}
