/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory;

public class DomainFactoryException extends RuntimeException {

	public DomainFactoryException(String message) {
		super(message);
	}

	public DomainFactoryException(Throwable cause) {
		super(cause);
	}
}
