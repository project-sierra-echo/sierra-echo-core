package echo.core.domain.factory;

public enum StrategyHint {

  NON_NULL_SOURCE,

  NULLABLE_SOURCE
}
