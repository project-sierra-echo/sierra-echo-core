package echo.core.domain.factory;

import echo.core.domain.stereotype.Domain;
import java.util.function.Function;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public interface DomainFactory {

  @Nullable
  <E, T extends Domain, R> R create(@NonNull CreationContext<E, T> creationContext,
      @NonNull Function<T, R> transformer);

  default <E, T extends Domain> T create(@NonNull CreationContext<E, T> creationContext) {
    return create(creationContext, Function.identity());
  }

  @Nullable
  <E, T extends Domain, R> R fromSource(@Nullable E source, @NonNull T target,
      @NonNull Function<T, R> transformer);

  @Nullable
  <E, T extends Domain, R> R fromSource(@Nullable E source, @NonNull Function<T, R> transformer);

  @Nullable
  <E, T extends Domain> T fromSource(@Nullable E source);

  @Nullable
  <T extends Domain> T fromArguments();

  @Nullable
  <T extends Domain, R> R fromArguments(@NonNull Function<T, R> transformer);
}
