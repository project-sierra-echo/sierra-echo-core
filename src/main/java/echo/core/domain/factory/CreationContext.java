package echo.core.domain.factory;

import echo.core.domain.stereotype.Domain;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public interface CreationContext<E, T extends Domain> {

  @Nullable
  E getSource();

  @NonNull
  ProducingStrategy<T> getProducingStrategy();

  @NonNull
  MappingStrategy<E, T> getMappingStrategy();

  @NonNull
  default StrategyHint getStrategyHint() {
    return StrategyHint.NON_NULL_SOURCE;
  }
}
