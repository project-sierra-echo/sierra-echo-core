/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.factory.CreationContext;
import echo.core.domain.factory.MappingStrategy;
import echo.core.domain.factory.ProducingStrategy;
import echo.core.domain.stereotype.Domain;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;

class FromArgsContext<T extends Domain> implements CreationContext<Object[], T> {

	private final MappingStrategy<Object[], T> argsFirstMapping;

	FromArgsContext(MappingStrategy<Object[], T> argsFirstMapping) {
		this.argsFirstMapping = argsFirstMapping;
	}

	@Override
	public Object[] getSource() {
		return FactoryArgumentsContextHolder.getArgs()
				.orElse(ArrayUtils.EMPTY_OBJECT_ARRAY);
	}

	@NonNull
	@Override
	@SuppressWarnings("unchecked")
	public ProducingStrategy<T> getProducingStrategy() {
		final CutStrategy strategy = FactoryArgumentsContextHolder.requireStrategy();

		return () -> (T) BeanUtils.instantiateClass(strategy.getDomainType());
	}

	@NonNull
	@Override
	public MappingStrategy<Object[], T> getMappingStrategy() {
		return argsFirstMapping;
	}
}
