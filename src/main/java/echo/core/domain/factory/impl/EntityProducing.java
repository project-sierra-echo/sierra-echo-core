/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.factory.ProducingStrategy;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import org.springframework.lang.NonNull;
import org.springframework.util.ReflectionUtils;

public class EntityProducing<T> implements ProducingStrategy<T> {

  private final Constructor<T> constructor;

  public EntityProducing(Class<T> entityType) throws NoSuchMethodException {
    this.constructor = ReflectionUtils.accessibleConstructor(entityType);
  }

  @NonNull
  @Override
  public T get() {
    try {
      return constructor.newInstance();
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException ex) {
      throw new IllegalStateException(ex);
    }
  }
}
