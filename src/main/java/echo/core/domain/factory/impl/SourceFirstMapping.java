/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.DomainDescriptor;
import echo.core.domain.Getter;
import echo.core.domain.Setter;
import echo.core.domain.factory.DomainFactoryException;
import echo.core.domain.factory.MappingStrategy;
import echo.core.domain.factory.service.DescriptorService;
import echo.core.domain.stereotype.Domain;
import java.util.Objects;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class SourceFirstMapping<E, T extends Domain> implements MappingStrategy<E, T> {

	private final DescriptorService descriptorService;

	public SourceFirstMapping(DescriptorService descriptorService) {
		this.descriptorService = descriptorService;
	}

	@NonNull
	@Override
	public T apply(@Nullable E source, @NonNull T target) {
		if (Objects.isNull(source)) {
			return target;
		}

		final DomainDescriptor targetDescriptor = descriptorService
				.getDescriptor(target.getClass());

		descriptorService.getSourceGetters(source.getClass()).stream()
				.map(getter -> new Mapping(getter, targetDescriptor))
				.forEach(mapping -> mapping.map(source, target));

		return target;
	}

	private class Mapping {

		private final Getter getter;
		private final Setter setter;

		public Mapping(Getter getter, DomainDescriptor targetDescriptor) {
			setter = targetDescriptor.requireSetter(getter.getPropertyName());
			this.getter = getter;
		}

		public void map(E source, T target) {
			try {
				setter.set(target, getter.get(source));
			} catch (Exception any) {
				throw new DomainFactoryException(any);
			}
		}
	}
}
