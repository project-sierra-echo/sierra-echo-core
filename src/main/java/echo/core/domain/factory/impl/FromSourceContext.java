/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.WithDomain;
import echo.core.domain.factory.DomainFactoryException;
import echo.core.domain.factory.MappingStrategy;
import echo.core.domain.factory.ProducingStrategy;
import echo.core.domain.stereotype.Domain;
import java.util.Objects;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.NonNull;

public class FromSourceContext<E, T extends Domain> extends SourceBased<E, T> {

	private final ProducingStrategy<T> producingStrategy;

	@SuppressWarnings("unchecked")
	protected FromSourceContext(E source, MappingStrategy<E, T> mappingStrategy) {
		super(source, mappingStrategy);

		final WithDomain annotation = AnnotationUtils.findAnnotation(source.getClass(), WithDomain.class);

		if (Objects.isNull(annotation)) {
			throw new DomainFactoryException(
					String.format("Unable to find %s on type %s", WithDomain.class, source.getClass()));
		}

		try {
			producingStrategy = (ProducingStrategy<T>) new EntityProducing<>(annotation.value());
		} catch (NoSuchMethodException e) {
			throw new DomainFactoryException(e);
		}
	}

	@NonNull
	@Override
	public ProducingStrategy<T> getProducingStrategy() {
		return producingStrategy;
	}
}
