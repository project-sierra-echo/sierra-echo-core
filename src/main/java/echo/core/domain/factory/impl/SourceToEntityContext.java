/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.factory.MappingStrategy;
import echo.core.domain.factory.ProducingStrategy;
import echo.core.domain.stereotype.Domain;
import org.springframework.lang.NonNull;

class SourceToEntityContext<E, T extends Domain> extends SourceBased<E, T> {

	private final T entity;

	SourceToEntityContext(E source, T entity, MappingStrategy<E, T> mappingStrategy) {
		super(source, mappingStrategy);
		this.entity = entity;
	}

	@NonNull
	@Override
	public ProducingStrategy<T> getProducingStrategy() {
		return () -> entity;
	}
}
