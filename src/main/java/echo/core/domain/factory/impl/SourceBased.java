/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.factory.CreationContext;
import echo.core.domain.factory.MappingStrategy;
import echo.core.domain.stereotype.Domain;
import org.springframework.lang.NonNull;

public abstract class SourceBased<E, T extends Domain> implements CreationContext<E, T> {

	private final E source;
	private final MappingStrategy<E, T> mappingStrategy;

	protected SourceBased(E source, MappingStrategy<E, T> mappingStrategy) {
		this.source = source;
		this.mappingStrategy = mappingStrategy;
	}

	@Override
	public E getSource() {
		return source;
	}

	@NonNull
	@Override
	public MappingStrategy<E, T> getMappingStrategy() {
		return mappingStrategy;
	}
}
