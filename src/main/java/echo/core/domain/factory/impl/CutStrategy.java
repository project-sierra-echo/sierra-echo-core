/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import static echo.core.AnnotationUtils.getSinceEmpty;
import static java.util.Map.entry;
import static java.util.Optional.ofNullable;
import static org.springframework.core.annotation.AnnotationUtils.getAnnotation;

import echo.core.domain.DomainContext;
import echo.core.domain.DomainDescriptor;
import echo.core.domain.Setter;
import echo.core.domain.factory.Arg;
import echo.core.domain.factory.Cutting;
import echo.core.domain.factory.DomainFactoryException;
import echo.core.domain.factory.FactoryCut;
import echo.core.domain.stereotype.Domain;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.lang.NonNull;

class CutStrategy {

	private static final Logger LOGGER = LoggerFactory.getLogger(CutStrategy.class);

	private final Class<? extends Domain> domainType;
	private final Map<Integer, Setter> setters;

	CutStrategy(@NonNull Method method, @NonNull DomainContext domainContext) {
		final CutIntrospect cutIntrospect = new CutIntrospect(method, domainContext);

		domainType = cutIntrospect.domainType;
		setters = cutIntrospect.setterMap;
	}

	Map<Integer, Setter> getSetters() {
		return setters;
	}

	Class<? extends Domain> getDomainType() {
		return domainType;
	}

	private static class CutIntrospect {

		private final Class<? extends Domain> domainType;
		private final DomainContext domainContext;
		private final Method cutMethod;

		private final Map<Integer, Setter> setterMap;

		private CutIntrospect(Method method, DomainContext domainContext) {
			this.domainContext = domainContext;
			cutMethod = method;

			final FactoryCut factoryCut = locateFactoryCut(method);

			domainType = locateDomainType(factoryCut, method);

			final Map<Integer, String> cutParams = locateCutParams(factoryCut, method);

			setterMap = toSetters(cutParams);
		}

		private static FactoryCut locateFactoryCut(Method method) {
			return ofNullable(getAnnotation(method, FactoryCut.class))
					.orElseThrow(() -> new DomainFactoryException(getSinceEmpty(method, FactoryCut.class)));
		}

		private Map<Integer, String> locateCutParams(FactoryCut factoryCut, Method method) {
			final Parameter[] parameters = method.getParameters();
			final Stream<Integer> boxed = IntStream.range(0, parameters.length).boxed();

			if (Cutting.ALL.equals(factoryCut.cutting())) {
				return collectParams(boxed, parameters);
			}

			return collectParams(
					boxed.filter(i -> AnnotatedElementUtils.hasAnnotation(parameters[i], Arg.class)),
					parameters);
		}

		private Map<Integer, String> collectParams(Stream<Integer> stream, Parameter[] parameters) {
			return stream
					.map(i -> toNamedEntry(i, parameters[i]))
					.collect(Collectors.toMap(Entry::getKey, Entry::getValue));
		}

		private Class<? extends Domain> locateDomainType(FactoryCut factoryCut,
				Method method) {
			final Class<? extends Domain> annotatedDomainType = factoryCut.withDomain()
					.value();

			if (!Domain.class.equals(annotatedDomainType)) {
				return annotatedDomainType;
			}

			return ofNullable(domainContext.getSubTypes(method.getReturnType()).get(0))
					.orElseThrow(() -> new DomainFactoryException(
							String.format("Unable to locate domain type for %s#%s", method.getDeclaringClass(),
									method.getName())));
		}

		private String getLog(int paramKey, String parameterName) {
			final Class<?> methodDeclaringClass = cutMethod.getDeclaringClass();
			final String methodName = cutMethod.getName();

			return String.format("[%s#%s(%s) -> %s.%s]", methodDeclaringClass,
					methodName,
					paramKey,
					domainType,
					parameterName);
		}

		private Map<Integer, Setter> toSetters(Map<Integer, String> cutParams) {
			log(cutParams);

			final DomainDescriptor domainDescriptor = domainContext.requireDescriptor(domainType);

			return cutParams.entrySet().stream()
					.collect(Collectors.toMap(Entry::getKey,
							entry -> domainDescriptor.requireSetter(entry.getValue())));
		}

		private Entry<Integer, String> toNamedEntry(Integer paramKey, Parameter param) {
			if (!param.isAnnotationPresent(Arg.class)) {
				return entry(paramKey, param.getName());
			}

			return entry(paramKey, getArgName(param));
		}

		private String getArgName(Parameter param) {
			return ofNullable(param.getDeclaredAnnotation(Arg.class))
					.map(Arg.class::cast)
					.map(Arg::value)
					.map(String.class::cast)
					.filter(StringUtils::isNotEmpty)
					.orElse(param.getName());
		}

		private void log(Map<Integer, String> cutParams) {
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace("\n\t{}",
						cutParams.entrySet().stream()
								.map(entry -> getLog(entry.getKey(), entry.getValue()))
								.collect(Collectors.joining("\n\t")));
			}
		}
	}
}
