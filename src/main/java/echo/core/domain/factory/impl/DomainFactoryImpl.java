package echo.core.domain.factory.impl;

import static java.util.function.Function.identity;

import echo.core.domain.factory.CreationContext;
import echo.core.domain.factory.DomainFactory;
import echo.core.domain.factory.MappingStrategy;
import echo.core.domain.factory.StrategyHint;
import echo.core.domain.stereotype.Domain;
import java.util.Objects;
import java.util.function.Function;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@SuppressWarnings("rawtypes")
public class DomainFactoryImpl implements DomainFactory {

  private final MappingStrategy sourceFirstMapping;
  private final MappingStrategy argsFirstMapping;

  public DomainFactoryImpl(@NonNull MappingStrategy sourceFirstMapping,
      @NonNull MappingStrategy argsFirstMapping) {
    this.sourceFirstMapping = sourceFirstMapping;
    this.argsFirstMapping = argsFirstMapping;
  }

  @Override
  public <E, T extends Domain, R> R create(@NonNull CreationContext<E, T> creationContext,
      @NonNull Function<T, R> transformer) {
    assertSource(creationContext.getSource(), creationContext.getStrategyHint());

    final T newInstance = creationContext.getProducingStrategy().get();

    return transformer.apply(creationContext.getMappingStrategy()
        .apply(creationContext.getSource(), newInstance));
  }

  @Nullable
  @Override
  public <E, T extends Domain, R> R fromSource(@Nullable E source,
      @NonNull T target,
      @NonNull Function<T, R> transformer) {
    return create(
        new SourceToEntityContext<>(source, target, getSourceFirstMapping()),
        transformer);
  }

  @Nullable
  @Override
  public <E, T extends Domain, R> R fromSource(@Nullable E source,
      @NonNull Function<T, R> transformer) {
    return create(new FromSourceContext<>(source, getSourceFirstMapping()), transformer);
  }

  @Override
  public <E, T extends Domain> T fromSource(E source) {
    return fromSource(source, identity());
  }

  @SuppressWarnings("unchecked")
  private <E, T extends Domain> MappingStrategy<E, T> getSourceFirstMapping() {
    return sourceFirstMapping;
  }

  @SuppressWarnings("unchecked")
  private <T extends Domain> MappingStrategy<Object[], T> getArgsFirstMapping() {
    return argsFirstMapping;
  }

  private void assertSource(Object source, StrategyHint strategyHint) {
    if (StrategyHint.NULLABLE_SOURCE.equals(strategyHint)
        || Objects.nonNull(source)) {
      return;
    }

    throw new IllegalStateException("Expecting creation source to be non-null");
  }

  @Override
  public <T extends Domain> T fromArguments() {
    return fromArguments(identity());
  }

  @Override
  public <T extends Domain, R> R fromArguments(@NonNull Function<T, R> transformer) {
    return create(new FromArgsContext<>(getArgsFirstMapping()), transformer);
  }
}
