/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import java.lang.reflect.Method;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.aspectj.lang.Signature;
import org.springframework.lang.NonNull;

class CutKey {

	private final String key;

	CutKey(@NonNull Method method) {
		key = String.format("%s %s.%s(%s)", method.getReturnType().getSimpleName(),
				method.getDeclaringClass().getName(),
				method.getName(),
				getArgsSignature(method));
	}

	CutKey(@NonNull Signature signature) {
		key = signature.toString();
	}

	private static String getArgsSignature(Method method) {
		return Stream.of(method.getParameterTypes())
				.map(Class::getSimpleName)
				.collect(Collectors.joining(","));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		CutKey cutKey = (CutKey) o;

		return new EqualsBuilder().append(key, cutKey.key).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(key).toHashCode();
	}
}
