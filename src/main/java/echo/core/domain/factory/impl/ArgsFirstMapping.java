/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.Setter;
import echo.core.domain.factory.MappingStrategy;
import echo.core.domain.stereotype.Domain;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class ArgsFirstMapping<T extends Domain> implements MappingStrategy<Object[], T> {

	@NonNull
	@Override
	public T apply(@Nullable Object[] arguments, @NonNull T entity) {
		if (ArrayUtils.isEmpty(arguments)) {
			return entity;
		}

		final CutStrategy strategy = FactoryArgumentsContextHolder.requireStrategy();

		strategy.getSetters()
				.forEach((argIndex, setter) -> setArg(arguments, entity, argIndex, setter));

		return entity;
	}

	private void setArg(Object[] arguments, T entity, Integer argIndex, Setter setter) {
		setter.set(entity, getSetterArg(
				arguments, argIndex));
	}

	private Object getSetterArg(Object[] arguments, Integer argIndex) {
		if (argIndex < arguments.length) {
			return arguments[argIndex];
		}

		return null;
	}
}
