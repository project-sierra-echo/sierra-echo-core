/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.DomainContext;
import echo.core.domain.factory.FactoryArgsProvider;
import echo.core.domain.factory.FactoryCut;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.lang.NonNull;
import org.springframework.util.ReflectionUtils;

public abstract class AbstractFactoryArgumentsAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(
			AbstractFactoryArgumentsAspect.class);

	private final Map<CutKey, CutStrategy> strategyMap;

	protected AbstractFactoryArgumentsAspect(@NonNull DomainContext domainContext,
			@NonNull String scannedPackage) {
		final List<Class<?>> argsProviderClasses = scanForProviders(scannedPackage);
		final List<Method> cutMethods = getCutMethods(argsProviderClasses);

		strategyMap = cutMethods.stream()
				.collect(Collectors.toMap(CutKey::new, method -> new CutStrategy(method, domainContext)));
	}

	private static Class<?> beanDefToProviderClass(BeanDefinition beanDef) {
		try {
			return Class.forName(beanDef.getBeanClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Stream<Method> factoryCutOnly(Method[] methods) {
		return Stream.of(methods)
				.filter(method -> method.isAnnotationPresent(FactoryCut.class));
	}

	private List<Method> getCutMethods(List<Class<?>> argsProviderClasses) {
		return argsProviderClasses.stream()
				.map(ReflectionUtils::getDeclaredMethods)
				.flatMap(AbstractFactoryArgumentsAspect::factoryCutOnly)
				.toList();
	}

	private List<Class<?>> scanForProviders(String scannedPackage) {
		final ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);

		scanner.addIncludeFilter(new AnnotationTypeFilter(FactoryArgsProvider.class));

		return scanner.findCandidateComponents(scannedPackage).stream()
				.map(AbstractFactoryArgumentsAspect::beanDefToProviderClass)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	@Pointcut("@annotation(echo.core.domain.factory.FactoryCut)")
	public void cutArguments() {
	}

	public void persistArguments(JoinPoint joinPoint) {
		FactoryArgumentsContextHolder.setArgs(joinPoint.getArgs());

		final CutStrategy strategy = resolveCutStrategy(joinPoint.getSignature());

		FactoryArgumentsContextHolder.setStrategy(strategy);
	}

	private CutStrategy resolveCutStrategy(Signature signature) {
		final CutStrategy strategy = strategyMap.get(new CutKey(signature));

		if (Objects.isNull(strategy)) {
			LOGGER.trace("Unable to locate factory strategy for {}", signature);
		}

		return strategy;
	}
}
