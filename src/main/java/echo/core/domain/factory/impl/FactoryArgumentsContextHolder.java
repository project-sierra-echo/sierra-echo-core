/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.impl;

import echo.core.domain.factory.DomainFactoryException;
import java.util.Objects;
import java.util.Optional;
import org.springframework.core.NamedThreadLocal;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public abstract class FactoryArgumentsContextHolder {

	private static final ThreadLocal<FactoryArgumentsContext> THREAD_LOCAL =
			new NamedThreadLocal<>("Factory arguments");

	private static final String ARGS = "args";
	private static final String STRATEGY = "strategy";

	private FactoryArgumentsContextHolder() {
    throw new UnsupportedOperationException();
  }

	@NonNull
	public static Optional<Object[]> getArgs() {
		return get(ARGS, Object[].class);
	}

	public static void setArgs(@Nullable Object[] args) {
		set(ARGS, args);
	}

	@NonNull
	public static Optional<CutStrategy> getStrategy() {
		return get(STRATEGY, CutStrategy.class);
	}

	public static void setStrategy(@Nullable CutStrategy strategy) {
		set(STRATEGY, strategy);
	}

	@NonNull
	public static CutStrategy requireStrategy() {
		return getStrategy()
				.orElseThrow(() -> new DomainFactoryException(
						String.format("Unable to locate %s", CutStrategy.class)));
	}

	@NonNull
  public static FactoryArgumentsContext get() {
    if (Objects.isNull(THREAD_LOCAL.get())) {
      THREAD_LOCAL.set(new FactoryArgumentsContext());
    }

    return THREAD_LOCAL.get();
  }

	@NonNull
  private static <T> Optional<T> get(String key, Class<T> type) {
    return Optional.ofNullable(get().get(key))
				.map(type::cast);
  }

	public static void set(FactoryArgumentsContext threadContext) {
     THREAD_LOCAL.set(threadContext);
  }

	private static void set(String key, Object value) {
		final FactoryArgumentsContext context = get();

		if (Objects.isNull(value)) {
			context.remove(key);
			return;
		}

		context.put(key, value);
  }

	public static void clear() {
    THREAD_LOCAL.remove();
  }
}
