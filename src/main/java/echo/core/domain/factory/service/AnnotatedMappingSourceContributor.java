/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.service;

import echo.core.Constants;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

public class AnnotatedMappingSourceContributor implements MappingSourceContributor {

	private static final Logger LOGGER = LoggerFactory.getLogger(
			AnnotatedMappingSourceContributor.class);

	private static Set<Class<?>> findSourceClasses() {
		final ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(
					false);

		scanner.addIncludeFilter(new AnnotationTypeFilter(FactorySource.class));

		return scanner.findCandidateComponents(Constants.BASE_PACKAGE).stream()
				.map(AnnotatedMappingSourceContributor::beanDefToClass)
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
	}

	private static Class<?> beanDefToClass(BeanDefinition beanDef) {
		try {
			return Class.forName(beanDef.getBeanClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("squid:S2629")
	private static void logEntries(Set<Class<?>> sourceClasses) {
		if (!LOGGER.isTraceEnabled()) {
			return;
		}

		if (sourceClasses.isEmpty()) {
			LOGGER.debug("No factory sources found");
			return;
		}

		LOGGER.trace("Found following factory sources:\n\t{}", StringUtils.join(sourceClasses, ','));
	}

	@Override
	public List<Class<?>> contribute() {
		final Set<Class<?>> sourceClasses = findSourceClasses();

		logEntries(sourceClasses);

		return List.copyOf(sourceClasses);
	}
}
