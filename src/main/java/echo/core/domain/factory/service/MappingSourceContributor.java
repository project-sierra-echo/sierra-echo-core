/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.service;

import java.util.List;

public interface MappingSourceContributor {

	List<Class<?>> contribute();
}
