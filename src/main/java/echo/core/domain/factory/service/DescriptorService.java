/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.service;

import echo.core.domain.DomainDescriptor;
import echo.core.domain.Getter;
import echo.core.domain.stereotype.Domain;
import java.util.List;
import org.springframework.lang.NonNull;

public interface DescriptorService {

	@NonNull
	List<Getter> getSourceGetters(@NonNull Class<?> sourceClass);

	@NonNull
	DomainDescriptor getDescriptor(@NonNull Class<? extends Domain> domainType);
}
