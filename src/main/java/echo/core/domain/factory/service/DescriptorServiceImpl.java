/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain.factory.service;

import static java.util.Collections.unmodifiableList;

import echo.core.Constants;
import echo.core.domain.DomainContext;
import echo.core.domain.DomainDescriptor;
import echo.core.domain.Getter;
import echo.core.domain.PropertyAccessor;
import echo.core.domain.Reader;
import echo.core.domain.factory.DomainFactoryException;
import echo.core.domain.stereotype.Domain;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.lang.NonNull;
import org.springframework.util.ReflectionUtils;

public class DescriptorServiceImpl implements DescriptorService {

	private final DomainContext domainContext;
	private final SourceGetterCache sourceGetters;

	public DescriptorServiceImpl(DomainContext domainContext,
			AutowireCapableBeanFactory beanFactory) {
		this.domainContext = domainContext;

		sourceGetters = new SourceGetterCache(beanFactory);
	}

	@NonNull
	@Override
	public List<Getter> getSourceGetters(@NonNull Class<?> sourceClass) {
		return Optional.ofNullable(sourceGetters.get(sourceClass))
				.orElseThrow(() -> new DomainFactoryException(
						String.format("Unable to locate getters for source %s", sourceClass)));
	}

	@NonNull
	@Override
	public DomainDescriptor getDescriptor(@NonNull Class<? extends Domain> domainType) {
		return domainContext.requireDescriptor(domainType);
	}

	private static class SourceGetterCache extends HashMap<Class<?>, List<Getter>> {

		private static final Logger LOGGER = LoggerFactory.getLogger(SourceGetterCache.class);
		private static final String GETTER_PREFIX = "get";

		private SourceGetterCache(AutowireCapableBeanFactory beanFactory) {
			discoverContributors(beanFactory).stream()
					.map(MappingSourceContributor::contribute)
					.flatMap(Collection::stream)
					.forEach(this::putEntries);

			forEach((key, getters) -> {
				put(key, unmodifiableList(getters));

				if (CollectionUtils.isEmpty(getters)) {
					LOGGER.warn("Source {} was registered without any getters", key);
					return;
				}

				LOGGER.trace("Source {} registered getters for the following properties: {}",
						key,
						getters.stream()
								.map(PropertyAccessor::getPropertyName)
								.collect(Collectors.joining(", ")));
			});
		}

		@SuppressWarnings("unchecked")
		private static Class<MappingSourceContributor> tryParseContributorType(
				BeanDefinition beanDefinition) {
			try {
				return (Class<MappingSourceContributor>) Class.forName(beanDefinition.getBeanClassName());
			} catch (ClassNotFoundException e) {
				throw new DomainFactoryException(e);
			}
		}

		private static boolean isGetter(Method method) {
			final String methodName = method.getName();

			return methodName.startsWith(GETTER_PREFIX) && !GETTER_PREFIX.equals(methodName)
					&& !Object.class.equals(method.getDeclaringClass());
		}

		private static Getter createReader(Method method) {
			return new Reader(getCamelCasedName(method), method);
		}

		private static String getCamelCasedName(Method method) {
			final String propertyNameCandidate = method.getName()
					.replaceFirst(GETTER_PREFIX, StringUtils.EMPTY);

			if (StringUtils.isEmpty(propertyNameCandidate)) {
				return propertyNameCandidate;
			}

			return String.format("%s%s",
					propertyNameCandidate.substring(0, 1).toLowerCase(),
					propertyNameCandidate.substring(1));
		}

		private void putEntries(Class<?> sourceClass) {
			ReflectionUtils.doWithMethods(sourceClass, method -> {
				putIfAbsent(sourceClass, new ArrayList<>());
				get(sourceClass).add(createReader(method));
			}, SourceGetterCache::isGetter);
		}

		private List<MappingSourceContributor> discoverContributors(
				AutowireCapableBeanFactory beanFactory) {
			final ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(
					false);

			scanner.addIncludeFilter(new AssignableTypeFilter(MappingSourceContributor.class));

			return scanner.findCandidateComponents(Constants.BASE_PACKAGE).stream()
					.map(SourceGetterCache::tryParseContributorType)
					.map(contributorType -> createContributor(contributorType, beanFactory))
					.toList();
		}

		private MappingSourceContributor createContributor(
				Class<MappingSourceContributor> contributorType,
				AutowireCapableBeanFactory beanFactory) {
			return beanFactory.createBean(contributorType);
		}
	}
}
