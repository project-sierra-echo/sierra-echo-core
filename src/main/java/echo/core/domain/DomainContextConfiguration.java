/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import org.springframework.context.annotation.Bean;

public class DomainContextConfiguration {

	@Bean
	public DomainContext domainContext() {
		return new DomainContextImpl();
	}
}
