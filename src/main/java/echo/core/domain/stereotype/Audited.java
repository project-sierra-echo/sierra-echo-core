package echo.core.domain.stereotype;

import java.time.LocalDateTime;

public interface Audited {

  LocalDateTime getCreationTimestamp();

  void setCreationTimestamp(LocalDateTime timestamp);

  EventSource getCreatedBy();

  void setCreatedBy(EventSource auditor);

  LocalDateTime getUpdateTimestamp();

  void setUpdateTimestamp(LocalDateTime timestamp);

  EventSource getUpdatedBy();

  void setUpdatedBy(EventSource auditor);
}
