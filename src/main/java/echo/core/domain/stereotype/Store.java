package echo.core.domain.stereotype;

public interface Store extends Domain, Audited {

  Long getId();

  void setId(Long id);

  String getCode();

  void setCode(String code);

  String getName();

  void setName(String name);
}
