package echo.core.domain.stereotype;

import java.time.LocalDateTime;

public interface Permanent {

  LocalDateTime getDeleteTimestamp();

  void setDeleteTimestamp(LocalDateTime timestamp);

  EventSource getDeletedBy();

  void setDeletedBy(EventSource auditor);
}

