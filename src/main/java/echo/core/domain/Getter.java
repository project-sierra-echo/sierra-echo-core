/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public interface Getter extends PropertyAccessor {

	@Nullable
	Object get(@NonNull Object source);
}
