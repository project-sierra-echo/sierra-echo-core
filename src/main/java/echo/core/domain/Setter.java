/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public interface Setter extends PropertyAccessor {

	void set(@NonNull Object target, @Nullable Object value);
}
