/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import static echo.core.domain.DescriptorUtils.requireDescriptor;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class Reader extends AbstractAccessor implements Getter {

	private final Method readerMethod;

	public Reader(Class<?> type, String propertyName) {
		this(propertyName, requireDescriptor(type, propertyName, PropertyDescriptor::getReadMethod));
	}

	public Reader(String propertyName, Method readerMethod) {
		super(propertyName);
		this.readerMethod = readerMethod;
	}

	@Nullable
	@Override
	public Object get(@NonNull Object source) {
		try {
			return readerMethod.invoke(source);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new DomainTypeException(e);
		}
	}
}
