/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import java.beans.PropertyDescriptor;
import java.util.Optional;
import java.util.function.Function;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;

public abstract class DescriptorUtils {

	private DescriptorUtils() {
		throw new UnsupportedOperationException();
	}

	public static <T> Optional<T> locateDescriptor(@NonNull Class<?> type,
			@NonNull String propertyName,
			@NonNull Function<PropertyDescriptor, T> mapper) {
		return Optional.ofNullable(BeanUtils.getPropertyDescriptor(type, propertyName))
				.map(mapper);
	}

	public static <T> T requireDescriptor(@NonNull Class<?> type,
			@NonNull String propertyName,
			@NonNull Function<PropertyDescriptor, T> mapper) {
		return locateDescriptor(type, propertyName, mapper)
				.orElseThrow(() -> new DomainTypeException(
						String.format("Property %s descriptor is required in type %s", propertyName, type)));
	}
}
