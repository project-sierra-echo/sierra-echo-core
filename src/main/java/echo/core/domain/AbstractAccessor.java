/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import org.springframework.lang.NonNull;

public abstract class AbstractAccessor implements PropertyAccessor {

	private final String propertyName;

	protected AbstractAccessor(String propertyName) {
		this.propertyName = propertyName;
	}

	@NonNull
	@Override
	public String getPropertyName() {
		return propertyName;
	}
}
