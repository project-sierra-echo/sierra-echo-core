/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

public class DomainTypeException extends RuntimeException {

	public DomainTypeException(String message) {
		super(message);
	}

	public DomainTypeException(Throwable cause) {
		super(cause);
	}
}
