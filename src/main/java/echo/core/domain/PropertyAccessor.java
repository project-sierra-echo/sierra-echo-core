/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import org.springframework.lang.NonNull;

public interface PropertyAccessor {

	@NonNull
	String getPropertyName();
}
