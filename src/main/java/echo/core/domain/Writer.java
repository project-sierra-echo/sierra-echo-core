/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class Writer extends AbstractAccessor implements Setter {

	private final Method writerMethod;

	public Writer(Class<?> type, String propertyName) {
		super(propertyName);
		writerMethod = DescriptorUtils.requireDescriptor(type, propertyName,
				PropertyDescriptor::getWriteMethod);
	}

	@Override
	public void set(@NonNull Object target, @Nullable Object value) {
		try {
			writerMethod.invoke(target, value);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new DomainTypeException(e);
		}
	}
}
