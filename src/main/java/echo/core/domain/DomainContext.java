/**
 * PROJECT: Sierra Echo
 * Author: Ngoc Huy
 */
package echo.core.domain;

import echo.core.domain.stereotype.Domain;
import java.util.List;
import java.util.Optional;
import org.springframework.lang.NonNull;

public interface DomainContext {

	@NonNull
	Optional<DomainDescriptor> getDescriptor(@NonNull Class<? extends Domain> domainType);

	@NonNull
	DomainDescriptor requireDescriptor(@NonNull Class<? extends Domain> domainType);

	@NonNull
	List<Class<? extends Domain>> getSubTypes(@NonNull Class<?> domainType);
}
